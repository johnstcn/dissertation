
\chapter{Conclusions}
\label{chapter:conclusions}

In this chapter, a brief summary of the work undertaken is provided (Section~\ref{conclusions:summary}), the various challenges encountered while carrying out this work are summarised (Section~\ref{conclusions:challenges}), and possible avenues of future work are outlined (Section~\ref{conclusions:future_work}).

\section{Summary}
\label{conclusions:summary}

In this work, an array of previous studies of the effects of CAVs in simulated scenarios were evaluated on numerous criteria, including the presence or absence of realistic vehicular simulation, realistic network simulation, and the complexity of the road network (Section~\ref{section:related_work}).

The studies evaluated which focus on the larger-scale effects of CAVs on traffic flow tended to simulate complex and/or realistic road networks, but omitted performing realistic network simulation. Conversely, the studies evaluated which focus on the performance and/or implementation of networked CAV models tended to choose less complex road networks, but as a rule will perform realistic network simulation to ensure that the CAV model performs well in a physical environment. However, few of these studies addressed the rate at which CAV models degrade with increased packet loss.

The hypothesis was posed (Section~\ref{section:research_question}) that realistic network simulation has no significant effect on the performance of CAVs in large realistic scenarios. A series of large-scale mixed-traffic simulations were designed to test this hypothesis, building on top of previous work by \citet{gueriau_quantifying_2020}.

These experiments (Section~\ref{section:experimental_setup}) involved a large realistic road network, realistic vehicle simulation coupled with realistic network simulation using state-of-the-art software, and varied the rate at which packets would be forcibly dropped. Traffic measures including Travel Rate and Congestion Index were measured on each edge of the simulated road network.

The results of the above simulations (Section~\ref{section:results}) were unable to refute the hypothesis: no significant effect on the travel rate of vehicles in the simulation was observed when the probability of packets being arbitrarily dropped was varied. Multiple issues were found in the experimental implementation, and identified as avenues for future work.

\section{Challenges}
\label{conclusions:challenges}

A number of challenges were encountered while completing this work:
\begin{itemize}
    \item The simulation software used is implemented in C++, which allows for great efficiency performing intensive simulations. However, C++ is a complex language in which it is easy to make programming errors.
    \item Both VEINS and PLEXE have little in the way of unit tests, meaning that contributors implementing new features must perform manual testing. In contrast, SUMO's codebase is well-tested and includes a number of automated integration tests. Manual testing is prone to human error; addition of more automated test suites would allow future contributors to iterate more rapidly while avoiding the addition of errors into the existing codebase.
    \item The simulation software used has a huge array of parameters to control every possible aspect of the simulation. These parameters are not all documented; many can only be found by inspecting the source code of the respective application. Changes to these parameters may result in unexpected emergent behaviour that may not arise in all scenarios, and recreating the circumstances under which such behaviour emerges is a time-consuming process. 
    \item Performing realistic network simulation on such a scale is extremely resource-intensive. For example, the simulations for the high-traffic scenario at 70\% MPR took over 48 hours of real time to complete. Additionally, an enormous volume of data is produced by simulations of such scale. In the case of the high-traffic scenario, over 39.5 million rows of data were produced. Analysing such a volume of data is a daunting task.
    \item Due to the single-threaded nature of the simulation software used, multiple simulations needed to be run in parallel. However, as explained in Section~\ref{section:experimental_setup:data_analysis}, emitting the required per-edge data directly from SUMO was not possible to perform in a parallel fashion. Therefore, the data needed to be emitted in a different format, and reconstructed afterwards. This was a non-trival task which involved transforming co-ordinates between the different reference systems of SUMO and OMNeT++.
\end{itemize}


\section{Future Work}
\label{conclusions:future_work}

Due to time constraints, a number of potential avenues of improvements were identified but not pursued in this work. These are enumerated here.
\begin{itemize}
    \item The simulation software used in this work is limited to single-threaded execution, and cannot take advantage of the multiple cores present in modern hardware. This work could increase simulation speeds by an order of magnitude.
    \item SUMO and VEINS' mode of commmunication incurs a large amount of overhead and a significant amount of waiting, as described in Section~\ref{section:experimental_setup:architecture}. Integrating both the vehicle and network simulation into the same execution context could obviate the requirement for lock-step parallel execution between SUMO and VEINS, and thereby greatly increase simulation speeds.
    \item The online platoon forming algorithm used in this work is extremely na\"ive and does not represent the state-of-the-art. Repeating this work with the addition of a state-of-the-art platoon formation algorithm would provide a more realistic result.
    \item The impact of lane-changing manoeuvres was not investigated in this work. These have a significant effect on traffic flow and stability. Authors such as \citet{an_lane-changing_2019} investigate the effects of co-ordinated lane-changing manoeuvres. As packet loss is likely to have a significant effect on such mechanisms, incorporating such research into this work would provide greater insights into the robustness of such algorithms in the face of communication degradation.
    \item The level of packet loss investigated was capped at 50\%. As other studies (e.g. \citet{santini_consensus-based_2017}) have shown, this may have been too low a level to cause significant degradation of longitudinal controller performance.
    \item The effects of packet loss on simulated surrogate safety measures, such as Post-Encroachment Time (PET) were not investigated in this work. As road traffic accidents claim a large number of human lives every year, this should also be investigated.
    \item The path loss model used in these experiments only factors in the distance between the sender and the receiver. Additional models may take other obstacles into account, such as buildings or other vehicles, when computing the success or failure of the delivery of a packet. This would further ground this work in reality.
    \item Two other scenarios were provided by \citet{gueriau_quantifying_2020}: a motorway scenario, and a city centre scenario. The city centre scenario, in particular, would be interesting to investigate. The high number of buildings and other obstacles would likely have a strong impact on the performance of simulated CAVs, assuming the use of the appropriate path loss model.
    \item Only vehicle-to-vehicle communication was investigated in this work. Investigating the impact of the addition of vehicle-to-infrastructure communications would be worthwhile. In non-line-of-sight scenarios, such as the city centre scenario, it is likely that infrastructure-based communications would be able to partially compensate for path loss between vehicles.
\end{itemize}