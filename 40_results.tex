
\chapter{Results}
\label{chapter:results}

As previous studies (e.g. \citet{makridis_connected_2018}) have shown, effects of CAVs are highly pronounced in dense traffic scenarios, and much less pronounced in low-density traffic due to the limited interactions between vehicles. Therefore, we consider the effect of PER on both traffic rate and congestion index in a free-flow low traffic scenario as a baseline, but ultimately investigate the effect of PER in a high-density traffic environment.

In this chapter, the results of the simulations detailed in Chapter~\ref{section:experimental_setup} are presented.  Section~\ref{section:results:lowtraffic} presents the effects of Packet Error Rate (PER) on the simulations at free-flow. Section~\ref{section:results:hightraffic} presents the effects of PER on the simulations in a congested state. Section~\ref{section:results:summary} presents a high-level summary and analysis of results from all scenarios.

\section{Low Traffic Scenario}
\label{section:results:lowtraffic}

Results for the Low Traffic scenario are summarised in Table \ref{fig:table:low_traffic_summary}. We can see very little difference in the numbers of vehicles inserted into the simulation, which indicates little to no congestion inside the road network. As the traffic is free-flow, there is no effect of increased PER. This is an expected result --- at low levels of traffic and market penetration rate, the likelihood of two CACC-enabled vehicles being within 50m of each other is low, and therefore the opportunities for forming platoons limited. Figure~\ref{fig:results:lowtraffic:boxplots} shows boxplots of average vehicle speed per edge, comparing the effects of PER on the controllers tested. We see no effects of packet error rate during this free-flow period.

\begin{figure}
     \footnotesize
     \centering
     \includegraphics[width=0.5\textwidth]{images/boxplot_lowtraffic_pdr_comparison.png}
     \label{fig:results:lowtraffic:boxplots}
     \caption{Comparison of the effects of PER on the average speed of CACC controllers in low traffic.}
\end{figure}

\begin{table}
\footnotesize
\centering
\begin{tabular}{c|l l l l l}
\hline
\textbf{Controller} & \textbf{MPR (\%)} & \textbf{PER (\%)} & \textbf{TR (min/km)} & \textbf{CI} & \textbf{N} \\
\hline
HDV & N/A & N/A & 0.710 & 0.032 & 1268 \\
\\
CACC & 20\% & 0\% & 0.704 & 0.020 & 1267\\
     &      & 50\% & 0.704 & 0.020 & 1267\\
     & 70\% & 0\% & 0.710 & 0.043 & 1271\\
     &      & 50\% & 0.710 & 0.043 & 1271\\
\\
PLOEG & 20\% & 0\% & 0.704 & 0.020 & 1267\\
     &      & 50\% &  0.704 & 0.020 & 1267\\
     & 70\% & 0\% & 0.710 & 0.043 & 1271\\
     &      & 50\% & 0.710 & 0.043 & 1271\\
\hline
\end{tabular}
\caption{Summary of results for Low Traffic scenario (from the time period 0400--0430), means over all edges of Packet Drop Rate (PER), Travel Rate, (TR), Congestion Index (CI), and vehicle count (N).}
\label{fig:table:low_traffic_summary}
\end{table}

As described previously in~\ref{section:experimental_setup:modifications}, the custom platooning scenario is configured to engage ACC with the ACC desired speed being equal to the maximum allowable on the network (100 km/h). This is the most likely cause of the larger proportion of vehicles travelling at the specific speed of \(27.78 m/s\) in this scenario.

\section{High Traffic Scenario}
\label{section:results:hightraffic}

Results for the High Traffic scenarios (0700--0730) are summarised in Table \ref{fig:table:high_traffic_summary}. We can see the effects of CAVs on traffic density by comparing the numbers of vehicles SUMO was able to insert into each simulation: at a 20\% MPR, it was possible to insert more vehicle into the simulation compared to the HDV baseline. 

We can also see the dramatically reduced congestion index (CI) for the scenarios in which CAVs are present, in comparison to those of HDVs. This is partially an artifact of the preconfigured default ACC speed detailed in Section~\ref{section:results:summary}. Remarkably, at a higher MPR, the number of vehicles inserted into the network dropped. The most likely explanation for this lies in the implementation of the na\"ive platooning strategy detailed in Section \ref{section:experimental_setup:modifications}, as the CACC-enabled vehicles initially drive in ACC mode at a preconfigured speed until a beacon is successfully received. 

\begin{figure}
     \centering
     \footnotesize
     \includegraphics[width=0.5\textwidth]{images/boxplot_hightraffic_pdr_comparison.png}
     \label{fig:results:hightraffic:boxplots}
     \caption{Comparison of the effects of PER on the average speed of CACC controllers in high traffic.}
 \end{figure}

No major differences are visible with regard to the effect of the packet error rate (PER) on travel rate (TR) and congestion index (CI) on both the PATH (CACC) and Ploeg controllers from this experiment. Figure~\ref{fig:results:hightraffic:boxplots} shows a comparison of the effects of PER on various controllers in the high traffic scenario. The PATH controller (CACC) is seen to be slightly more sensitive to the effects of PER, but this effect is small at the higher market penetration rate.

\begin{table}
\footnotesize
\centering
\begin{tabular}{c|l l l l l}
\hline
\textbf{Controller} & \textbf{MPR (\%)} & \textbf{PER (\%)} & \textbf{TR (min/km)} & \textbf{CI} & \textbf{N} \\
\hline
HDV & N/A & N/A & 2.787 & 2.91 & 9098 \\
\\
CACC & 20\% & 0\% & 1.181 & 0.78 & 9266 \\
     &      & 50\% & 1.181 & 0.78 & 9266 \\
     & 70\% & 0\% & 1.002 & 0.495 & 8591 \\
     &      & 50\% & 0.997 & 0.485 & 8735 \\
\\
PLOEG & 20\% & 0\% & 1.181 & 0.78 & 9266 \\
     &      & 50\% & 1.181 & 0.78 & 9266 \\
     & 70\% & 0\% & 0.996 & 0.485 & 8735 \\
     &      & 50\% & 0.996 & 0.485 & 8735 \\
\hline
\end{tabular}
\caption{Summary of results for High Traffic scenario (from the time period 0700--0730), means over all edges of Packet Drop Rate (PER), Travel Rate, (TR), Congestion Index (CI), and vehicle count (N).}
\label{fig:table:high_traffic_summary}
\end{table}

The traffic flow can be visualised more easily by plotting each edge of the network with colour mark corresponding to the average travel rate of vehicles along each edge. Figure~\ref{fig:results:hightraffic_comparison_mpr70_pdr_0_50} compares the per-edge travel rates of the PATH CACC and Ploeg controllers at 70\% MPR with 0\% forced PER, and with 50\% forced PER. The bottlenecks on particular edges are clearly visible, but no significant changes in travel rate are visible with increased PER. 

\begin{sidewaysfigure}
     \footnotesize
     \centering
     \includegraphics[width=0.49\textwidth]{results/edgedata_HighTraffic_8_25200_27000_tr.png}
     \includegraphics[width=0.49\textwidth]{results/edgedata_HighTraffic_9_25200_27000_tr.png}
     \includegraphics[width=0.49\textwidth]{results/edgedata_HighTraffic_10_25200_27000_tr.png}
     \includegraphics[width=0.49\textwidth]{results/edgedata_HighTraffic_11_25200_27000_tr.png}
     \caption{Comparison of 0\% vs 50\% PER on Travel Rates of PATH CACC and Ploeg controller at high traffic, 70\% MPR.}
     \label{fig:results:hightraffic_comparison_mpr70_pdr_0_50}
\end{sidewaysfigure}

\section{Summary}
\label{section:results:summary}

The results as presented above appear to show that the effect of PER on the performance of longitudinal CACC controllers (CAVs) is nonexistent, and we are thus unable to refute the hypothesis raised in Section~\ref{section:research_question}.

However, this should not be interpreted to mean that realistic network simulation has no effect on the performance of CAVs, as other work has shown this to not be the case in smaller-scale simulations. Most dramatically, work by \citet{van_der_heijden_analyzing_2017} has shown in simulation that malicious actors are able to effect vehicle collisions by performing actions on the network, while work by \citet{santini_consensus-based_2017} has shown that longitudinal controllers become string-unstable at high PERs.

A number of issues are evident with the experimental scenario as presented:
\begin{itemize}
     \item Per \citet{santini_consensus-based_2017}, noticeable degradation of other controllers only occurred at PERs above 60\%. It is entirely possible that the chosen upper bound for forced PER in this experiment was too conservative.
     \item The mechanism in which CACC-enabled vehicles choose the leading vehicle (\ref{section:experimental_setup:modifications}) is sub-optimal. Additionally, due to the CACC-enabled vehicles initially engaging ACC at a preconfigured speed, the overall speed distributions for all scenarios are effectively artificially distorted. This should be addressed in future work.
     \item An additional oversight exists in the results: there is no logging of the percentage of time a CACC-enabled vehicle spends in CACC mode, versus ACC mode or HDV mode. This makes it difficult to gauge the effect of packet loss on longitudinal controller performance.
     \item Multiple merge bottlenecks are present in the road network used, but the effects of lane-changing manoeuvres are not taken into account in this study. This should be addressed in future work.
\end{itemize}

Additional graphs and data are available separately in the linked Git repository: \url{https://gitlab.com/johnstcn/dissertation}.
