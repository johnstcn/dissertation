PDFLATEX = ./latexdockercmd.sh pdflatex
BIBTEX = ./latexdockercmd.sh bibtex
TEXCOUNT = ./latexdockercmd.sh texcount

all : clean latex

clean :
	rm -f *.pdf *.out *.lot *.lof *.log *.aux *.bbl *.blg *.fdb_latexmk *.fls *.synctex.gz *.toc *.tdo


latex : thesis.tex
	$(PDFLATEX) -f thesis
	$(BIBTEX) thesis
	$(PDFLATEX) -f thesis
	$(PDFLATEX) -f thesis

.PHONY: wc
wc:
	$(TEXCOUNT) -inc -incbib -sum thesis.tex
