### Cian's Masters Thesis

Run `make` to build.

Link to latest version built in GitLab CI/CD can be accessed [here](https://gitlab.com/johnstcn/dissertation/-/jobs/artifacts/master/file/thesis.pdf?job=build)

Actual published version is available on the [TCD SCSS website](https://www.scss.tcd.ie/publications/theses/diss/2020/TCD-SCSS-DISSERTATION-2020-087.pdf).
